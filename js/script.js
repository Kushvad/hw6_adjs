const getIPBtn = document.querySelector('.get-ip');
const viewIP = document.querySelector('.set-ip');
const content= document.querySelector('.content');

async function getIP() {
  try {
    const response = await fetch('https://api.ipify.org/?format=json');
    const { ip } = await response.json();
    setIP(ip);
    getAddress(ip);
  } catch (error) {
    console.log(error);
  }
}

async function getAddress(ip) {
  try {
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,lat,lon`);
    const data = await response.json();
    setAddress(data);
  } catch (error) {
    console.log(error);
  }
}

function setAddress({ continent, country, regionName, city, lat, lon }) {
  content.innerHTML = `
        Continent: ${continent}
        <br> Country: ${country}
        <br> Region: ${regionName}
        <br> City: ${city}
        <br> Latitude: ${lat}
        <br> Longitude: ${lon}`
}

function setIP(ip) {
  viewIP.innerText = ip;
}

getIPBtn.addEventListener('click', getIP);